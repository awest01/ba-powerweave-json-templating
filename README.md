# Templating JSON Objects

Full example can be found in [dist/index.html](dist/index.html) and [dist/js/bundle.js](dist/js/bundle.js)

#### Returned JSON Structure

```json
{
    "total_products": 27,
    "page_size": 6,
    "current_page": 2,
    "products": [
        {
            "sku": "ABC123",
            "description": "ABC 123 Description",
            "prices": [
                {
                    "price_was": null,
                    "price_now": 5.00,
                }
            ]
        },
        {
            "sku": "ABC555",
            "description": "ABC 555 Description",
            "section_icons": [
                {
                    "icon": "https://www.brand-estore.com/site/new.png",
                    "alt":  "New Product",
                },
                {
                    "icon": "https://www.brand-estore.com/site/sale.png",
                    "alt":  "SALE!!!",
                },
            ],
            "prices": [
                {
                    "price_was": 12.95,
                    "price_now": 10.00,
                }
            ],
        },
        ...
        ..
        ..
    ]
}
```


### Templating Returned JSON

There are various libraries that can be used to help in templating JSON returned from your controller, I suggest using [Handlebars.JS](https://handlebarsjs.com) for a couple of reasons;

1. It's lightweight 
2. Enable simple logic & loops, 
3. Templates produced are clean and easily readable.

Below is a simple example of a handlebars template, with loops & basic logic applied

Couple of things of note:

1. All handlebars templates **MUST** have a type of `text/x-handlebars-template`
2. All handlebars templates **MUST** be in the `<body>`

#### Rough Document Template
```html
<html>
    <head>
        <title>Product Page</title>
        ...
        ..
    </head>

    <body>
        <div class="filters">
            ...
            ..
            ..
        </div>

        <div class="product-list">
            <div id="product-list-container">
                <!-- Compiled HTML Will be injected into THIS container -->
            </div>
        </div>

        <script type="x-handlebars-template" id="product-template">
            // product template code
        </script>

        <script type="x-handlebars-template" id="filters-template">
            // filter template code
        </script>
    </body>
</html>
```

##### Product Template

```html
<script type="text/x-handlebars-template" id="x-product-template">
    <!-- Loop through the products array -->
    {{#each products}}
        <div class="product_data">
            <div class="product_image">
                <img src="{{ thumbnail }}" />
            </div>
    
            <div class="product_details">
                <h3>{{ sku }}</h3>
                <p>{{ description }}</p>
            </div>
    
            <div class="product_prices">
                <div class="price price_was">{{ prices.price_was }}</div>
                <div class="price price_now">{{ prices.price_now }}</div>
            </div>

            <!-- Example of simple logic being applied -->
            {{#if section_icons.length}}
                <div class="product_section_icons">
                    {{#each section_icons}}
                        <img src="{{ icon }}" alt="{{ alt }}" />
                    {{/each}}
                </div>
            {{/if}}
        </div>
    {{/each}}
</script>
```
#### Parsing template into HTML

```js
// Get the TEXT value of script element.
var template = document.getElementById('x-product-template').innerHTML;

// Compile the template to accept JSON 
var compiledTemplate = Handlebars.compile(template);

// Run your usual AJAX calls and parse the response JSON
$.getJSON('/path/to/controller.json', function(repsonse){
    var parsedTemplateHTML = compiledTemplate(response);
});
```