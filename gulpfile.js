const gulp         = require('gulp'),
      pug          = require('gulp-pug'),
      sass         = require('gulp-sass'),
      concat       = require('gulp-concat'),
      postcss      = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      fs           = require('fs'),
      data         = require('gulp-data');; 

const browserSync = require('browser-sync').create();

function createProducts(){
    let products = {
        "total":    0,
        "products": []
    };


    for (let i = 0; i <= 200; i++){
        var title = i.toString().padStart(3, '0');
        var price_was = (i % 25) === 0 ? 5.00 : null;
        var price_now = 5.00;

        let product = {
            "sku":   "TEST" + title,
            "image": "https://via.placeholder.com/200x200/000/fff",
            "description": "Test Product #" + title,
            "price": {
                "price_was": price_was,
                "price_now": price_now
            }
        };

        if ((i % 33) === 0){
            product.has_logic = "something";
        }

        products.products.push(product);
    }

    products.total = products.products.length;

    fs.writeFile('./src/assets/products-generated.json', JSON.stringify(products), (err) => {
        return true;
    });
}

function moveAssets(){
    return gulp.src('./src/assets/**/*')
        .pipe(gulp.dest('dist/assets/'));
}

function buildTemplates(){
    return gulp.src('src/*.pug')
        .pipe(data(function(file){
            return JSON.parse(fs.readFileSync('./src/data.json'));
        }))
        .pipe(pug({
            pretty: true,
            filters: 
            {
                "readfile": function(file, options){
                    console.log(options);
                    console.log(file)
                }
            }
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
}

function buildScripts(){
    return gulp.src('./src/js/*.js')
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('dist/js/'))
        .pipe(browserSync.stream());
}

function buildStyles(){
    return gulp.src("src/**/*.scss")
            .pipe(sass().on('error', sass.logError))
            .pipe(postcss([autoprefixer()]))
            .pipe(gulp.dest('dist/'))
            .pipe(browserSync.stream());
}

exports.generate = createProducts;
exports.build = gulp.series(buildTemplates, buildStyles, moveAssets, buildScripts);
exports.watch = () => {
    browserSync.init({
        server: {
            baseDir: 'dist/',
        }
    });

    gulp.watch('src/assets/**/*', moveAssets);
    gulp.watch('src/js/*.js',     buildScripts);
    gulp.watch('src/**/*.pug',    buildTemplates);
    gulp.watch('src/**/*.scss',   buildStyles);
};