$(function(){
    // 
    // Setup templates:
    // Note the innerHTML at the end, handlebars templates require 
    // you pass them a string
    //--------------------------------------------------------------------------
    var template = document.getElementById('x-product-template').innerHTML;
    var compiledTemplate = Handlebars.compile(template);

    //
    // Proceed with usual JS logic
    //--------------------------------
    var currentPage = 0;
    var pageSize    = 12;

    function loadMoreProducts(){
        var productJson = fauxLazy(currentPage++, pageSize);
        var compiledHTML = compiledTemplate(productJson);

        $('#x-product-list-container').append(compiledHTML);
    }

    loadMoreProducts();

    // Simple infinite scroll
    $(window).on('scroll', debounce(() => {
        if ($(window).scrollTop() >= ( $(document).height() - $(window).height() - 10 )){
            loadMoreProducts();
        }
    }, 200));
});

//
// Don't pay to much attention to this
// Code below is mainly for simulation
//--------------------------------------
function fauxLazy(currentPage, pageSize, totalItems = 250){
    var skuPrefix = "ABC";
    var startingPosition = pageSize * currentPage;
    var products = [];

    for (var i = startingPosition; i < Math.min(startingPosition + pageSize, totalItems); i++){
        var sku = "ABC" + i.toString().padStart(3, '0');
        var price = getPrice();
        var price_was = (Math.ceil(price) % 10) === 0 ? 12.85 : null;

        var product = {
            "sku": sku,
            "thumbnail": "http://via.placeholder.com/200/" + getColour() + "/fff",
            "description": "Lipsum ABC " + sku,
            "prices": {
                "price_was": price_was,
                "price_now": getPrice(),
            }
        }

        console.log(product);

        products.push(product);
    }

    return {
        "total_products": totalItems,
        "current_page": currentPage,
        "page_size": pageSize,
        "products": products,
    };
};

function getColour() {
    var letters = '0123456789ABCDEF';
    var color  = "";

    for (var i = 0; i < 3; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }

    return color;
}

function getPrice(){
    return Math.round(Math.random() * 10, 2);
}

function debounce(func, wait, immediate) {
    var timeout;

    return function executedFunction() {
        var context = this;
        var args    = arguments;

        var later = function() {
            timeout = null;
                
            if (!immediate) func.apply(context, args);
        };

        var callNow = immediate && !timeout;
    
        clearTimeout(timeout);
        
        timeout = setTimeout(later, wait);

        if (callNow){
            func.apply(context, args);
        } 
    };
};